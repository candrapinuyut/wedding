<?php
include("config.php");
if($_SESSION['login']==1){
 $id=isset($_GET['id'])?$_GET['id']:null;
$act=isset($_GET['act'])?$_GET['act']:null;

if($act=='allow' && $id!=null){
  $sql = Sql::Allow($sql,$id);
  header('location:/komen.php');

}
if($act=='delete' && $id!=null){
  $sql = Sql::Delete($sql,$id);
  header('location:/komen.php');

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" type='text/css' href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

  <div class="container">
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
      <!-- Brand -->
      <a class="navbar-brand" href="#">Tania Hamza Wedding</a>

      <!-- Links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Komentar</a>
        </li>
      </ul>
    </nav>
    <div class="card mt-5">
      <div class="card-body">
        <div class="card-title">Semua Komentar</div>
        <hr />

        <?php
        $q = Sql::All2($sql);
        foreach($q as $d){

          $name =  $d['name'];
          $text =  $d['isi'];
          $date =  $d['date'];
          $st   =  $d['status'];

        if($st==0){
        ?>
        <div class="card">
          <div class="card-body">
            <div class="card-title"><?=$name;?> <span class="small text-muted"><?=$date;?> </span></div>
            <p class='text-muted'>
              <?=$text;?>
            </p>
            <a href='?act=allow&id=<?=$d['id'];?>'class="btn btn-primary">terima</a>
            <a href='?act=delete&id=<?=$d['id'];?>' class="btn btn-danger">tolak</a>
          </div>
        </div>
        <?php
        }
        else if($st==1){
        ?>
        <div class="card mt-3">
          <div class="card-body">
            <div class="card-title"><?=$name;?> <span class="small text-muted"><?=$date;?> </span></div>
            <p class='text-muted'>
              <?=$text;?>
            </p>
            <div class="float-right">
              <span class='small' style='color:green !important'>Status: Diterima</span>
            </div>
          </div>
        </div>
      <?php
      } else if ( $st==2 ) {
      ?>
        <div class="card mt-3">
          <div class="card-body">
            <div class="card-title"><?=$name;?> <span class="small text-muted"><?=$date;?> </span></div>
            <p class='text-muted'>
              <?=$text;?>
            </p>
            <div class="float-right">
              <span class='small' style='color:red !important'>Status: Ditolak</span>
            </div>
          </div>
        </div>
        <?php
        }
      }
        ?>


      </div>
    </div>

   </div>


  <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js'></script>
</body>
</html>
<?php
}else{
  header('location:/login.php');
}
?>
