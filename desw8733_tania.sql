-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 04 Des 2020 pada 11.10
-- Versi server: 10.2.34-MariaDB-cll-lve
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `desw8733_tania`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `komentar`
--

CREATE TABLE `komentar` (
  `id` int(8) NOT NULL,
  `name` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `komentar`
--

INSERT INTO `komentar` (`id`, `name`, `isi`, `date`, `status`) VALUES
(8, 'Erlannga R.', 'Selamat Berbahagia Nurtania Pasambuna Dan Pasangan. Bahagia Dunia AKhirat. aamiin', '2020-09-30 07:52:08', 1),
(17, 'Sandi Gomba', 'Halo Mantan Teman', '2020-09-30 07:53:28', 0),
(18, 'hai', 'hai', '2020-09-30 08:01:00', 2),
(19, 'Hay', 'Jude', '2020-10-04 00:36:04', 0),
(20, 'Dinda Andjaly Mokoginta', 'Happy wedding kak Taniaa, smoga sakinah mawadah, warahmah Aamiiin', '2020-10-18 14:35:55', 1),
(21, 'Pingkan harmony', 'Selamat Tania cantik soleha.\nInsyaAllah bahagia dunia akhirat dpat keturunan anak yg shalih shalihah ðŸ¤ \n', '2020-10-18 14:36:13', 1),
(22, 'Prof Dr Ir H Hafidz Darwis SKom MT', 'â€œMudah-mudahan Allah memberkahimu, baik ketika senang maupun susah dan selalu mengumpulkan kamu berdua pada kebaikan.â€ (HR. Abu Dawud)', '2020-10-18 14:36:26', 1),
(23, 'Yunita Mokoginta', 'Happy Wedding Kak Tan, May your marriage life will be filled with joyful and bless. Be happy till the your hair turn white!! CheersðŸ¥°', '2020-10-18 14:36:47', 1),
(24, 'Graldi setiawan korompot', 'Semoga sakinah mawadah warohmah kka intan dan suami ðŸ˜‡ðŸ™ðŸ¼', '2020-10-18 14:36:35', 1),
(25, 'Dimas Dirgantara Ranti', 'Tania...\nSelamat menjadi istri dari bapak dokter...\nJadi keluarga sakinah mawaddah warahmah..aamiin\nBahagia selalu selamat, berjodoh sampai maut memisahkan, dan semoga cepat di berikan momongan yg soleh-solehah', '2020-10-18 14:37:09', 1),
(26, 'Hegrisaro', 'Selamat Tania dan Suami ðŸ™ðŸ¼\nBahagia selalu sampe kakek nenek ðŸ§“ðŸ‘µ\nSemoga selalu diberkati Tuhan yang maha kuasa ðŸ˜‡', '2020-10-18 14:37:18', 1),
(27, 'Gita Mamonto', 'Finallyyyy!! Selamat berbahagiaaaa kk sayang & suami semoga Samawa dan lancar sampe hari H yaaa ðŸ˜ðŸ’ðŸ’œ', '2020-10-18 14:37:35', 1),
(28, 'Anenda Putri', 'Masha Allah.. selamat menempuh hidup baru Tania bersama Suami.. Insha Allah menjadi keluarga skainnah mawaddah warahmah.. Bahagia selalu ðŸ¥°', '2020-10-19 01:37:34', 1),
(29, 'Thisa Amelia', 'yuhuuu Finally kakak lunat ðŸ˜ðŸ’ bahagia selalu kakak sayang dan suami ðŸ¥°â¤ï¸', '2020-10-19 01:37:43', 1),
(30, 'Jya', 'MasyaAllah.. sakinah mawaddah warahmah syg ðŸ¥°', '2020-10-19 01:37:57', 1),
(31, 'Ersitta Tabo', 'Yeahhh alhamdulillah selamat menjadi istri wanita solehahKuh..\nSemoga menjadi keluarga sakinah mawaddah wa rohmah dn mendapat keturunan yg sholeh dan sholehah.. aamiin ðŸ˜˜ðŸ˜˜', '2020-10-19 01:37:51', 1),
(32, 'Ersitta Tabo', 'Yeahhh alhamdulillah selamat menjadi istri wanita solehahKuh..\nSemoga menjadi keluarga sakinah mawaddah wa rohmah dn mendapat keturunan yg shalih dan shalihah.. aminn ðŸ˜˜ðŸ˜˜', '2020-10-19 01:55:10', 1),
(33, 'Aris Zulhadji', '\"Baarakallahu laka wa baarakaa alaika wa jamaa bainakumaa fii khoirin\" ðŸ˜‡ðŸ™ smoga lancar sampai hari H tat', '2020-10-19 01:38:04', 1),
(34, 'Inka Pratiwi Mamonto', 'Alhamdulillah \nAkhirnya ðŸ˜‡ðŸ¤—\nSelamat berbahagia Bay & suami ðŸ¥°\nInsya Allah Lancar smpe hari H ðŸ˜‡ðŸ™ðŸ»', '2020-10-19 01:38:11', 1),
(35, 'Sandi', 'HAPPY WEDDING ANDALAN ðŸ¤©ðŸ¤©ðŸ¤©ðŸ¤©ðŸ˜…', '2020-10-19 01:55:05', 1),
(36, 'Wulandari', 'MasyaAllah. Berjalanlah terus hingga jannah.. Selamat berbahagia Tani sayaang', '2020-10-19 01:59:15', 0),
(37, 'Bobi', 'Selamat berbahagia ne, insya allah dimudahkan sampai hari H-nya nanti dan semoga menjadi keluarga SAMAWA ne', '2020-10-19 02:09:22', 0),
(38, 'Paman Uwa', 'Alhamdulilah, selamat menempuh bahtera rumah tangga Tanss, semoga dilancarkan selalu segala niat baiknya dengan pak dokter, pokoknya bae2 samua hal aminðŸ™ðŸ™ðŸ’ƒðŸŽŠ', '2020-10-19 11:18:25', 1),
(39, 'Vey Pasambuna', 'Selamat menempuh hidup baru utat\nSakinah mawaddah warahma\nBismillah Sah ðŸ˜‡ðŸ¥³', '2020-10-19 16:06:21', 0),
(40, 'Ika baranoy', 'Alhamdulillah bnyak slmat kak tan\nSmoga menjdi istri yg baik dan menjadi keluarga yg SAMAWAðŸ™ðŸ»ðŸ˜‡ðŸ¥°ðŸ¥°', '2020-10-19 16:20:04', 0),
(41, 'Rudini', 'SAMAWA,Bahagia Dunia Akhirat ðŸ˜‡ðŸ˜‡ðŸ˜‡ðŸ™', '2020-10-20 15:43:12', 0),
(42, 'Tyologi', 'Selamat memasuki babak baru Tanss & suami ðŸ™ selalulah akur, hidup baik bahagia dalam waktu lama. Segala yang terbaik torang semogakan buat kalian yak ðŸ˜ŠðŸ˜‡', '2020-10-21 02:56:53', 0),
(43, 'Danny Talot', 'Selamat Menempuh Bahtera Rumah Tangga Tania. ðŸ™ðŸ™ðŸ™ðŸ™ðŸ™ðŸ™', '2020-10-21 05:15:40', 0),
(44, 'Gally mokoagow', 'Maha Besar Allah.. selamat berlayar di kehidupan baru bersama suami.ðŸ™ SAMAWA bay\n\nYou\'re the queen !:)', '2020-10-21 07:01:57', 0),
(45, 'Ichal mamonto', 'Alhamdulillah akhirnyaa smpee di tahap ini. Banyak slamat dan bahagiaa sllu obayku sayang smoga jadi kel.samawa â¤ï¸ðŸ¥°', '2020-10-21 07:39:26', 0),
(46, 'Wanna Hambali', 'Selamat berbahagia sayangkuu, semoga dijadikan keluarga yg samawa,dianugerahkan keturunan yg sholeh sholehah\nðŸ˜‡ðŸ¤— so sorry cant be there, kiss kiss from semarangðŸ˜˜ðŸ˜˜ðŸ˜˜', '2020-10-21 16:38:25', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
